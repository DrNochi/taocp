### Notes on the Exercises

#### 1. [00] What does the rating "M20" mean

An exercise rated M20 is a mathematically oriented problem of average difficulty. It should require about 15 minutes to solve.

#### 2. [10] Of what value can the exercises in a textbook be to the reader?

Exercises can help the reader check, if the he understood what he read, deepen his understanding by making him reflect and show him how to apply the gained knowledge to concrete problems.
