def gcd(m, n):
    while True:
        r = m % n
        if r == 0:
            return n
        m = n
        n = r

import unittest

class EuclidTest(unittest.TestCase):
    def test(self):
        self.assertEqual(gcd(119, 544), 17)
        self.assertEqual(gcd(544, 119), 17)
        self.assertEqual(gcd(544, 544), 544)
        self.assertEqual(gcd(199, 1), 1)

if __name__ == "__main__":
    unittest.main()
